<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
</head>
<body>
	<center>
 	<table>
 		<tr>
 			<th>Edit</th>
 		</tr>
 	</table>
 	<?php
 	  include('includes/c_function.php');
 	  if(isset($_POST['btn_update'])){
 	  	$data = array(
	    	'fname' => $_POST['fname'],
		    'mname' => $_POST['mname'],
		    'lname' => $_POST['lname'],
		    'birthday' => $_POST['birthday'],
		    'age' => $_POST['age'],
		    'gender' => $_POST['gender'],
		    'address' => $_POST['address'],
		    'status'=> ($_POST['status']) ? 1 : 0
	    	);
 	  	$hdd_id = $_POST['hdd_id'];
 	  	$where = "where rec = $hdd_id";
	   	$result = UpdateData('register', $data, $where);
	   	if($result){
	   		$message = 'Successfuly Update!!!';
	   	}else{
	   		$message = "Failed To update!!!";
	   	}
	   	redirect("index.php?message=$message");
 	  }
 	  $id = $_GET['id'];
 	  $data = getData('register', "where rec = $id", 'order by rec asc');
 	  $row = mysql_fetch_assoc($data);
 	  
 	?>
 	  <form action="" method="post" autocomplete="off">
	 	<table border="0" style="text-align: left">
	 		<tr><th>FirstName:</th>
	 			<input type="hidden" name="hdd_id" value="<?php echo $row['rec']?>">
	 			<td><input type="text" name="fname" value="<?php echo $row['fname']?>"></td>
	 		</tr>
	 		<tr><th>Middle Name:</th>
	 			<td><input type="text" name="mname" value="<?php echo $row['mname']?>"></td>
	 		</tr>
	 		<tr><th>Last Name:</th>
	 			<td><input type="text" name="lname" value="<?php echo $row['lname']?>"></td>
	 		</tr>
	 		<tr><th>Birthday:</th>
	 			<td><input type="text" name="birthday" placeholder="yyyy-mm-dd" value="<?php echo $row['birthday']?>"></td>
	 		</tr>
	 		<tr><th>Age:</th>
	 			<td><input type="number" name="age" value="<?php echo $row['age']?>" ></td>
	 		</tr>
	 		<tr><th>Gender:</th>
	 			<td>
	 				<select name="gender">
	 					<option value="Male" <?php echo ($row['gender'] == 'Male') ? 'selected' : ''?> >Male</option>
	 					<option value="Female" <?php echo ($row['gender'] == 'Female') ? 'selected' : ''?>>Female</option>
	 				</select>
	 			</td>
	 		</tr>
	 		<tr><th>Address:</th>
	 			<td>
	 				<textarea name="address"><?php echo $row['address']?></textarea>
	 			</td>
	 		</tr>
	 		<tr><th>Status:</th>
	 			<td><input type="checkbox" name="status" value="1" <?php echo ($row['status'] == '1') ? 'checked' : '';?> ></td>
	 		</tr>
	 		<tr><td></td><td><input type="submit" name="btn_update" value="Update">
	 			<a href="index.php">Cancel</a>
	 		</td>
	 		</tr>
	 	</table>
	 </form>
 </center>
</body>
</html>