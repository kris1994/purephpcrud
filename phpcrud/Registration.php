<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
</head>
<body>
	<center>
 	<table>
 		<tr>
 			<th>Register</th>
 		</tr>
 	</table>
 	  <form action="includes/register_function.php" method="post" autocomplete="off">
	 	<table border="0" style="text-align: left">
	 		<tr><th>FirstName:</th>
	 			<td><input type="text" name="fname"></td>
	 		</tr>
	 		<tr><th>Middle Name:</th>
	 			<td><input type="text" name="mname"></td>
	 		</tr>
	 		<tr><th>Last Name:</th>
	 			<td><input type="text" name="lname"></td>
	 		</tr>
	 		<tr><th>Birthday:</th>
	 			<td><input type="text" name="birthday" placeholder="yyyy-mm-dd"></td>
	 		</tr>
	 		<tr><th>Age:</th>
	 			<td><input type="number" name="age"></td>
	 		</tr>
	 		<tr><th>Gender:</th>
	 			<td>
	 				<select name="gender">
	 					<option value="Male">Male</option>
	 					<option value="Female">Female</option>
	 				</select>
	 			</td>
	 		</tr>
	 		<tr><th>Address:</th>
	 			<td>
	 				<textarea name="address"></textarea>
	 			</td>
	 		</tr>
	 		<tr><th>Status:</th>
	 			<td><input type="checkbox" name="status" value="1"></td>
	 		</tr>
	 		<tr><td></td><td><input type="submit" name="btn_save" value="Save"></td></tr>
	 	</table>
	 </form>
 </center>
</body>
</html>